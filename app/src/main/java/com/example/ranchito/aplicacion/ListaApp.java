package com.example.ranchito.aplicacion;
import android.graphics.drawable.Drawable;


    public class ListaApp {

        private String name;
        Drawable icon;

        public ListaApp(String name, Drawable icon) {
            this.name = name;
            this.icon = icon;
        }

        public String getName() {
            return name;
        }

        public Drawable getIcon() {
            return icon;
        }
    }

