package com.example.ranchito.aplicacion;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {
    private static final String TAG ="Inicio" ;
    Button regis1,ingre1;
    EditText Correo1, Pass1;
   private FirebaseAuth mAuth;
    private ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Correo1 =(EditText)findViewById(R.id.correo);
        Pass1=(EditText) findViewById(R.id.contra);
        ingre1=(Button) findViewById(R.id.Ingreso);
        mAuth = FirebaseAuth.getInstance();
        regis1=(Button)findViewById(R.id.Regi);
        regis1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =  new Intent(MainActivity.this, Registro.class);
                startActivity(intent);
            }
        });
        progressDialog =new ProgressDialog(this);
        ingre1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startIniciarSesion();

            }
        });
    }

    private void startIniciarSesion() {


        String email = Correo1.getText().toString().trim();
        String password = Pass1.getText().toString().trim();




        if (TextUtils.isEmpty(email)){
            Toast.makeText(this,"Ingrese un email",Toast.LENGTH_LONG).show();
            return;

        }

        if(TextUtils.isEmpty(password)){
            Toast.makeText(this,"Ingrese Contraseña",Toast.LENGTH_LONG).show();
            return;

        }


        progressDialog.setMessage("Iniciando");
        progressDialog.show();


        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Intent intent =new Intent (MainActivity.this, Todo.class);
                             startActivity(intent);
                        } else
                            Toast.makeText(MainActivity.this,"Usuario Incorrecto", Toast.LENGTH_LONG).show(); {
                            if (task.getException() instanceof FirebaseAuthUserCollisionException)
                            Toast.makeText(MainActivity.this,"No se pudo reguistrar el usuario", Toast.LENGTH_LONG).show();
                        }
                        progressDialog.dismiss();

                        // ...
                    }
                });




    }


    }



