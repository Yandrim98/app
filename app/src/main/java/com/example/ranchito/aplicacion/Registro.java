package com.example.ranchito.aplicacion;

import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;

import org.w3c.dom.Text;

public class Registro extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private EditText nombre, Correo2, password2;
     private Button  Registrar2;
    private ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        nombre =(EditText)findViewById(R.id.usuario);
        Correo2 =(EditText)findViewById(R.id.correoRegistro);
        password2=(EditText) findViewById(R.id.contraseñaRegistro);
        Registrar2=(Button) findViewById(R.id.Registrsr);
        mAuth = FirebaseAuth.getInstance();
      progressDialog =new ProgressDialog(this);
      Registrar2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               startRegistrarUsuario();
            }
        });
    }

    private void startRegistrarUsuario() {


        String email = Correo2.getText().toString().trim();
        String password = password2.getText().toString().trim();
        String user = nombre.getText().toString().trim();



        if (TextUtils.isEmpty(email)){
            Toast.makeText(this,"Ingrese un email",Toast.LENGTH_LONG).show();
              return;

        }

        if(TextUtils.isEmpty(password)){
            Toast.makeText(this,"Ingrese Contraseña",Toast.LENGTH_LONG).show();
             return;

        }
        if(TextUtils.isEmpty(user)){
            Toast.makeText(this,"Ingrese nombre",Toast.LENGTH_LONG).show();
            return;

        }

        progressDialog.setMessage("Registrando usuario..");
        progressDialog.show();


        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(Registro.this, "Se ha registrado el email", Toast.LENGTH_LONG).show();

                        } else{
                            if (task.getException() instanceof FirebaseAuthUserCollisionException){
                            Toast.makeText(Registro.this, "El usuario ya existe", Toast.LENGTH_LONG).show();

                    }else {
                                Toast.makeText(Registro.this, "No se pudo reguistrar el usuario", Toast.LENGTH_LONG).show();
                            }
                            }
                        progressDialog.dismiss();

                        // ...
                    }
                });


}


}